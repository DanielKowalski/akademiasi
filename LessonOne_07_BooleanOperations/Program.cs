﻿using System;

namespace LessonOne_07_BooleanOperations
{
    class Program
    {
        static void Main(string[] args)
        {
            // OR alternatywa
            Console.Write("true || true = "); 
            Console.WriteLine(true || true);
            
            Console.Write("true || false = "); 
            Console.WriteLine(true || false);
            
            Console.Write("false || false = ");
            Console.WriteLine(false || false);
            
            Console.Write("false || true = ");
            Console.WriteLine(false || true);
            
            // AND koniunkcja
            Console.Write("true && true = ");
            Console.WriteLine(true && true);
            
            Console.Write("true && false = ");
            Console.WriteLine(true && false);
            
            Console.Write("false && false = ");
            Console.WriteLine(false && false);
            
            Console.Write("false && true = ");
            Console.WriteLine(false && true);
            
            // NOT Negacja
            Console.Write("!true = ");
            Console.WriteLine(!true);
            
            Console.Write("!false = ");
            Console.WriteLine(!false);
            
            // XOR alternatywa rozłączna (wykluczająca)
            Console.Write("true ^ true = ");
            Console.WriteLine(true ^ true);
            
            Console.Write("true ^ false = ");
            Console.WriteLine(true ^ false);
            
            Console.Write("false ^ false = ");
            Console.WriteLine(false ^ false);
            
            Console.Write("false ^ true = ");
            Console.WriteLine(false ^ true);

            // Porównanie
            Console.Write("false == false = ");
            Console.WriteLine(false == false);
            
            Console.Write("true != true = ");
            Console.WriteLine(true != true);
            
            Console.Write("true != false = ");
            Console.WriteLine(true != false);
            
            Console.Write("false != false = ");
            Console.WriteLine(false != false);
            
            Console.Write("false != true = ");
            Console.WriteLine(false != true);
            
            // NAND zaprzeczenie koniunkcji
            Console.WriteLine(!(true && false));
            
            Console.WriteLine(!true && false); // false && false
            
            Console.Write("1 < 2 | 2 != 2 = ");
            Console.WriteLine(1 < 2 | 2 != 2);
            
            Console.Write("1 < 2 || 2 != 2 = ");
            Console.WriteLine(1 < 2 || 2 != 2);

            Console.Write("1 > 2 & 1 == 1 =");
            Console.WriteLine(1 > 2 & 1 == 1);
            
            Console.Write("1 > 2 && 1 == 1 =");
            Console.WriteLine(1 > 2 && 1 == 1);
        }
    }
}