﻿using System;
using System.Globalization;
using System.Threading;

namespace LessonOne_09_ConversionAndCasting
{
    class Program
    {
        static void Main(string[] args)
        {
            // Konwersja
            int x = 1;
            double y = 2;

            Console.WriteLine($"Konwersja int na double: {(x / y).GetType()}");

            int a = 1;
            long b = 2;

            Console.WriteLine($"Konwersja int na long {(a + b).GetType()}");

            byte c = 200;
            int d = 1;

            Console.WriteLine($"Konwersja byte na int {(c - d).GetType()}");

            //short e = d;

            string text1 = "2";
            //int a = text1;

            // Rzutowanie
            byte e = (byte) d;
            
            Console.WriteLine($"Rzutowanie int na byte {e}, {e.GetType()}");

            int f = 257;
            byte g = (byte) f;
            Console.WriteLine($"Rzutowanie int na byte przekroczenie zakresu: {g}");

            // 11111111 = 255
            // 1 | 00000000 = 0 
            // 100000001 = 257
            // 1 | 00000001 = 1

            int h = 1000;
            byte i = (byte) h;
            
            // 1111101000 = 100
            // 11 | 11101000 = 232
            
            Console.WriteLine($"Rzutowanie int na byte wielokrotne przekroczenie zakresu: {i}");

            Console.WriteLine($"Rzutowanie double na int: {(int) 2.532}");
            Console.WriteLine($"Rzutowanie double na float: {(float) 2.123456789101112}");
            
            // Zaokrąglanie
            Console.WriteLine(Math.Round(2.532));
            
            string j = "2";
            //int k = (int)j;

            int k = int.Parse(j);
            Console.WriteLine($"Parsowanie string na int: {k}");

            //Console.WriteLine($"Parsowanie stringa, który nie jest liczbą na int: {int.Parse("ala")}");
            
            //Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("pl-PL");
            Console.WriteLine($"Parsowanie string na decimal wg ustawień komputera: {decimal.Parse("2,3")}");
            Console.WriteLine($"Parsowanie string na decimal invariant: {decimal.Parse("2,3", CultureInfo.InvariantCulture)}");
            Console.WriteLine($"Parsowanie string na decimal invariant: {decimal.Parse("2.3", CultureInfo.InvariantCulture)}");

            Console.WriteLine($@"Parsowanie ""true"" na bool: {bool.Parse("true")}");
            Console.WriteLine($@"Parsowanie ""false"" na bool: {bool.Parse("false")}");

            Console.Write("Podaj wiek: ");
            string input = Console.ReadLine();

            int myAge = 21;
            int age = int.Parse(input);
            Console.WriteLine($"Róznica wieku między nami to {Math.Abs(myAge - age)} lat.");
        }
    }
}