﻿using System;

namespace LessonOne_06_NumericalOperations
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("12 + 33 = ");
            Console.WriteLine(12 + 33);
            Console.Write("12.5 + 22.3 = ");
            Console.WriteLine(12.5 + 22.3);
            Console.Write("12 - 233 = ");
            Console.WriteLine(12 - 233);
            Console.Write("12 - 0.75 = ");
            Console.WriteLine(12 - 0.75);
            Console.Write("2 * 3 = ");
            Console.WriteLine(2 * 3);
            Console.Write("0.03 * 0.69 = ");
            Console.WriteLine(0.03 * 0.69);
            Console.Write("0.03m * 0.69m = ");
            Console.WriteLine(0.03m * 0.69m);
            Console.Write("1 / 2 = ");
            Console.WriteLine(1 / 2);
            Console.Write("1 / 2.0 = ");
            Console.WriteLine(1 / 2.0);
            Console.Write("3 + 3 * 2 = ");
            Console.WriteLine(3 + 3 * 2);
            Console.Write("(3 + 3) * 2 = ");
            Console.WriteLine((3 + 3) * 2);
            Console.Write("2 * (1 - 3 * 2 + 3) = ");
            Console.WriteLine(2 * (1 - 3 * 2 + 3));
            Console.Write("2 * (1 - 3 * (2 + 3)) = ");
            Console.WriteLine(2 * (1 - 3 * (2 + 3)));

            Console.Write("3 % 2 = ");
            Console.WriteLine(3 % 2);
            Console.Write("3 % 3 = ");
            Console.WriteLine(3 % 3);

            // Inkrementacja
            int a = 1;
            Console.Write("a++: ");
            Console.WriteLine(a++); // a = a + 1; a += 1;
            Console.Write("a: ");
            Console.WriteLine(a);
            Console.Write("++a: ");
            Console.WriteLine(++a);
            Console.Write("a: ");
            Console.WriteLine(a);

            // Dekrementacja
            int b = 33;
            Console.Write("b--: ");
            Console.WriteLine(b--);
            Console.Write("b: ");
            Console.WriteLine(b);   
            Console.Write("--b: ");
            Console.WriteLine(--b);
            Console.Write("b: ");
            Console.WriteLine(b);

            double c = 2;
            c += 1.5; // c = c + 1.5;
            Console.Write("c += 1.5: ");
            Console.WriteLine(c);
            
            c -= 0.5; // c = c - 0.5;
            Console.Write("c -= 0.5: ");
            Console.WriteLine(c);
            
            c *= 3.5; // c = c * 3.5;
            Console.Write("c *= 3.5: ");
            Console.WriteLine(c);
            
            c /= 2; // c = c / 2;
            Console.Write("c /= 2: ");
            Console.WriteLine(c);

            c %= 2; // c = c % 2;
            Console.Write("c %= 2: ");
            Console.WriteLine(c);
            
            Console.Write("2 > 4: ");
            Console.WriteLine(2 > 4);
            Console.Write("2 < 4: ");
            Console.WriteLine(2 < 4);
            Console.Write("2 <= 4: ");
            Console.WriteLine(2 <= 4);
            Console.Write("3 >= 3: ");
            Console.WriteLine(3 >= 3);
            Console.Write("3 > 3: ");
            Console.WriteLine(3 > 3);
            Console.Write("2 <= 2: ");
            Console.WriteLine(2 <= 2);
            Console.Write("2 < 2: ");
            Console.WriteLine(2 < 2);
            Console.Write("2 == 2: ");
            Console.WriteLine(2 == 2);
            Console.Write("1 == 2: ");
            Console.WriteLine(1 == 2);

            double x = 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1;
            double y = 0.1 * 6;
            Console.WriteLine(x == y);

            decimal xd = 0.1m + 0.1m + 0.1m + 0.1m + 0.1m + 0.1m;
            decimal yd = 0.1m * 6;
            Console.WriteLine(xd == yd);

            Console.Write("2 do potęgi 3 = ");
            Console.WriteLine(Math.Pow(2, 3));

            Console.Write("PI = ");
            Console.WriteLine(Math.PI);
        }
    }
}