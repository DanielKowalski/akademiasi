﻿using System;

namespace LessonOne_10_Var
{
    class Program
    {
        static void Main(string[] args)
        {
            // Typ niejawny
            var x = 1;
            var y = 1L;
            var z = "test";
            var a = 1.2;
            var b = 1.2m;
            var c = 1.2f;
            var d = 'a';
            var e = 1 / 2;
            var f = 1 / 2.0;
            
            Console.WriteLine($"Typ 1 to {x.GetType()}");
            Console.WriteLine($"Typ 1L to {y.GetType()}");
            Console.WriteLine($@"Typ ""test"" to {z.GetType()}");
            Console.WriteLine($"Typ 1.2 to {a.GetType()}");
            Console.WriteLine($"Typ 1.2m to {b.GetType()}");
            Console.WriteLine($"Typ 1.2f to {c.GetType()}");
            Console.WriteLine($"Typ 'a' to {d.GetType()}");
            Console.WriteLine($"Typ 1 / 2 to {e.GetType()}");
            Console.WriteLine($"Typ 1 / 2.0 to {f.GetType()}");

            var g = 1;
            //g = int.MaxValue + 2L; 
            //g = int.MaxValue + 2;
            
            var h = 1L;
            //h = int.MaxValue + 2;
            h = int.MaxValue + 2L;

            var i = 132.2;
            //i = "test";
        }
    }
}