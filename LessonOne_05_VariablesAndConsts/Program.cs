﻿namespace LessonOne_05_VariablesAndConsts
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 1;
            const int b = 2;

            a = 33;
            //b = 12;

            int x;  // deklaracja
            
            // Kod
            
            x = 12; // inicjalizacja

            // nie można rozdzielić deklaracji i inicjalizacji dla stałych
            // const int y;
        }
    }
}