﻿using System;

namespace LessonOne_08_TextOperations
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "Ala";
            string b = "ma";
            string c = "kota";
            
            Console.WriteLine(a + b + c);
            Console.WriteLine(a + " " + b + " " + c);

            // Interpolacja stringów
            Console.WriteLine($"{a} {b} {c}");

            Console.WriteLine("Litwo! Ojczyzno maja! Ty jesteś jak zdrowie,"
            + "Ile cię trzeba cenić, ten tylko się dowie,"
            + "Kto cię stracił. Dziś piękność twą w całej ozdobie"
            + "Widzę i opisuję, bo tęsknię po tobie"
            + "Panno święta, co Jasnej bronisz Częstochowy"
            + "I w Ostrej świecisz Bramie! Ty, co gród zamkowy"
            + "Nowogródzki ochraniasz z jego wiernym ludem!"
            + "Jak mnie dziecko do zdrowia powróciłaś cudem,"
            + "(Gdy od płaczącej matki pod Twoją opiekę"
            + "Ofiarowany, martwą podniosłem powiekę"
            + "I zaraz mogłem pieszo do Twych świątyń progu"
            + "Iść za wrócone życie podziękować Bogu),"
            + "Tak nas powrócisz cudem na Ojczyzny łono."
            + "Tymczasem przenoś moją duszę utęsknioną"
            + "Do tych pagórków leśnych, do tych łąk zielonych,"
            + "Szeroko nad błękitnym Niemnem rozciągnionych;"
            + "Do tych pól malowanych zbożem rozmaitem,"
            + "Wyzłacanych pszenicą, posrebrzanych żytem;"
            + "Gdzie bursztynowy świerzop, gryka jak śnieg biała,"
            + "Gdzie panieńskim rumieńcem dzięcielina pała,"
            + "A wszystko przepasane jakby wstęgą, miedzą"
            + "Zieloną, na niej z rzadka ciche grusze siedzą.");

            Console.WriteLine(@"Litwo! Ojczyzno maja! Ty jesteś jak zdrowie,
                Ile cię trzeba cenić, ten tylko się dowie,
                Kto cię stracił. Dziś piękność twą w całej ozdobie
                Widzę i opisuję, bo tęsknię po tobie
                Panno święta, co Jasnej bronisz Częstochowy
                I w Ostrej świecisz Bramie! Ty, co gród zamkowy
                Nowogródzki ochraniasz z jego wiernym ludem!
                Jak mnie dziecko do zdrowia powróciłaś cudem,
                (Gdy od płaczącej matki pod Twoją opiekę
                Ofiarowany, martwą podniosłem powiekę
                I zaraz mogłem pieszo do Twych świątyń progu
                Iść za wrócone życie podziękować Bogu),
                Tak nas powrócisz cudem na Ojczyzny łono.
                Tymczasem przenoś moją duszę utęsknioną
                Do tych pagórków leśnych, do tych łąk zielonych,
                Szeroko nad błękitnym Niemnem rozciągnionych;
                Do tych pól malowanych zbożem rozmaitem,
                Wyzłacanych pszenicą, posrebrzanych żytem;
                Gdzie bursztynowy świerzop, gryka jak śnieg biała,
                Gdzie panieńskim rumieńcem dzięcielina pała,
                A wszystko przepasane jakby wstęgą, miedzą
                Zieloną, na niej z rzadka ciche grusze siedzą.");

            Console.WriteLine($@"  {a}

                        {b}
            
            {c}");

            Console.WriteLine("C:\\Users\\Daniel\\test.txt");
            Console.WriteLine(@"C:\Users\Daniel\test.txt");

            Console.WriteLine("\"ala\" ma kota");
            Console.WriteLine(@"""ala"" ma kota");

            string z = "zażółć gęślą jaźń";
            string z2 = "ZażÓłĆ gęślĄ jaźń";
            
            Console.WriteLine(z.ToUpper());
            Console.WriteLine(z2.ToLower());

            z = z.ToUpper();
            
            Console.WriteLine(z);
            Console.WriteLine(z2);

            string x = "To jest zielony dom.";

            Console.WriteLine(x.Replace("zielony", "czerwony"));
            Console.WriteLine(x);
        }
    }
}