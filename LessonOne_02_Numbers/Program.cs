﻿using System;

namespace LessonOne_02_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            byte minByte = 0;
            byte maxByte = 255;
            
            short minShort = -32768;
            short maxShort = 32767;

            int minInt = -2147483648;
            int maxInt = 2147483647;

            long minLong = -9223372036854775808;
            long maxLong = 9223372036854775807;

            sbyte minSByte = -128;
            sbyte maxSbyte = 127;

            ushort minUShort = 0;
            ushort maxUShort = 65535;

            uint minUInt = 0;
            uint maxUInt = 4294967295;

            ulong minULong = 0;
            ulong maxULong = 18446744073709551615;

            Byte a = 255; // byte
            Int16 b = 32767; // short
            Int32 c = 2147483647; // int
            Int64 d = 9223372036854775807; // long
            SByte e = 127; // sbyte
            UInt16 f = 65535; // ushort
            UInt32 g = 4294967295; // uint
            UInt64 h = 18446744073709551615; // ulong
            
            Console.WriteLine(123.GetType()); // int
            Console.WriteLine(123L.GetType()); // long

            long x = 123;
            Console.WriteLine(x.GetType()); // long

            float i = 1.12345678911f;
            double j = 1.12345678911;
            decimal k = 1.12345678911m;

            Console.WriteLine(i); // 1.1234568
            Console.WriteLine(j); // 1.12345678911
            Console.WriteLine(k); // 1.12345678911
            
            Single l = 2.12345678911F; // float
            Double m = 12.12345678911121314; // double
            Decimal n = 12.123456789111121314M; // decimal

            Console.WriteLine(l); // 2.1234567
            Console.WriteLine(m); // 12.123456789111213
            Console.WriteLine(n); // 12.123456789111121314

            double xd = 99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999.2;
            
            Console.WriteLine(xd); // 1E+308
            Console.WriteLine(decimal.MaxValue); // 79228162514264337593543950335
            Console.WriteLine(79228162514264337593543950335.123m); // 79228162514264337593543950335
            Console.WriteLine(79228162514264337593543950.123m); // 79228162514264337593543950.123
            
            // Ciekawostka:
            int xy = 0xFF;
            Console.WriteLine(xy); // 255
        }
    }
}